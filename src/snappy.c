#include <mruby.h>
#include <mruby/string.h>
#include <snappy-c.h>

static mrb_value
mrb_snappy_compress (mrb_state * mrb, mrb_value self)
{
  size_t csize;
  mrb_value ret;

  if (!mrb_string_p (self))
    mrb_raise (mrb, E_RUNTIME_ERROR, "cannot compress non string receiver");
  csize = snappy_max_compressed_length (RSTRING_LEN (self));
  ret = mrb_str_new_capa (mrb, csize);
  switch (snappy_compress
	  (RSTRING_PTR (self), RSTRING_LEN (self), RSTRING_PTR (ret), &csize))
    {
    case SNAPPY_OK:
      RSTR_SET_LEN (mrb_str_ptr (ret), csize);
      return ret;
    case SNAPPY_INVALID_INPUT:
      mrb_raise (mrb, E_RUNTIME_ERROR, "SNAPPY INVALID INPUT");
    case SNAPPY_BUFFER_TOO_SMALL:
      mrb_raise (mrb, E_RUNTIME_ERROR, "SNAPPY BUFFER TOO SMALL");
    }
  return mrb_nil_value ();
}

static mrb_value
mrb_snappy_uncompress (mrb_state * mrb, mrb_value self)
{
  size_t usize;
  mrb_value ret;

  if (!mrb_string_p (self))
    mrb_raise (mrb, E_RUNTIME_ERROR, "cannot uncompress non string receiver");
  if (snappy_uncompressed_length
      (RSTRING_PTR (self), RSTRING_LEN (self), &usize) != SNAPPY_OK)
    mrb_raise (mrb, E_RUNTIME_ERROR,
	       "receiver is not snappy compressed string");
  ret = mrb_str_new_capa (mrb, usize);
  if (snappy_uncompress (RSTRING_PTR (self), RSTRING_LEN (self),
			 RSTRING_PTR (ret), &usize) != SNAPPY_OK)
    mrb_raise (mrb, E_RUNTIME_ERROR,
	       "receiver is not snappy compressed string");
  RSTR_SET_LEN (mrb_str_ptr (ret), usize);
  return ret;
}

void
mrb_mruby_snappy_gem_init (mrb_state * mrb)
{
  mrb_define_method (mrb, mrb->string_class, "snappy", mrb_snappy_compress,
		     MRB_ARGS_NONE ());
  mrb_define_method (mrb, mrb->string_class, "unsnappy",
		     mrb_snappy_uncompress, MRB_ARGS_NONE ());
}

void
mrb_mruby_snappy_gem_final (mrb_state * mrb)
{
}
