MRuby::Gem::Specification.new('mruby-snappy') do |spec|
  spec.license = 'MIT'
  spec.authors = 'mako10k'
  spec.summary = 'mruby snappy compression / decompression'
  spec.version = '0.0.1'
  spec.linker.libraries << ['snappy']
end
